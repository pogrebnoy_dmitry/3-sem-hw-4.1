package org.sample;

import org.openjdk.jmh.annotations.*;

import java.io.File;
import java.io.IOException;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;

@Fork(1)
@Warmup(iterations = 3)
@Measurement(iterations = 6)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MILLISECONDS)

public class MyBenchmark {
    @State(Scope.Thread)
    public static class MyState {
        @Setup(Level.Trial)
        public void doSetup() {
            taskCounter = new TaskCounter();
            pagesAlredyVisit = new ConcurrentSkipListSet <>();
        }
        @TearDown(Level.Iteration)
        public void doTearDown() {
            File loadpages = new File("C:/Users/Дима/HW_4.1/trash");
            try {
                FileUtils.cleanDirectory(loadpages);
            } catch (IOException exp) {
                exp.printStackTrace();
            }
        }

        TaskCounter taskCounter;
        ConcurrentSkipListSet<String> pagesAlredyVisit;
        String url = "http://dluciv.name/";
    }
    @Benchmark
    public void test_1Tread_1Depth(MyState state) {
        Main.CrawlerTest(state.url, 1, 1, state.taskCounter, state.pagesAlredyVisit);
    }
    @Benchmark
    public void test_2Treads_1Depth(MyState state) {
        Main.CrawlerTest(state.url, 1, 2, state.taskCounter, state.pagesAlredyVisit);
    }
    @Benchmark
    public void test_4Treads_1Depth(MyState state) {
        Main.CrawlerTest(state.url, 1, 4, state.taskCounter, state.pagesAlredyVisit);
    }
    @Benchmark
    public void test_8Treads_1Depth(MyState state) {
        Main.CrawlerTest(state.url, 1, 8, state.taskCounter, state.pagesAlredyVisit);
    }
}