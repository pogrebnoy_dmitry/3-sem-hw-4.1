package org.sample;

import java.util.*;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        System.out.println("Enter the initial URL website");
        String initUrl = input.nextLine();
        System.out.println("Enter the maximum crawl depth");
        final int MAX_DEPTH = input.nextInt();


        /*
        String initUrl = "http://dluciv.name/";
        //example for crawl  http://gokrk.ru/active-citizens/tim-yunior/    https://edu.dluciv.name/  http://dluciv.name/
        final int MAX_DEPTH = 1;
        */

        CrawlerImpl(initUrl, MAX_DEPTH, 4);
    }

    public static void CrawlerImpl(String initUrl, final int MAX_DEPTH, final int NUM_OF_THREADS) {
        ConcurrentSkipListSet<String> pagesAlredyVisit = new ConcurrentSkipListSet <>();
        final ExecutorService executor = Executors.newFixedThreadPool(NUM_OF_THREADS);
        TaskCounter taskCounter = new TaskCounter();

        taskCounter.inc();
        executor.execute(new Crawler(initUrl, taskCounter, pagesAlredyVisit,executor,0,MAX_DEPTH));

        while (taskCounter.getCount() != 0) {
            try {
                Thread.sleep(1);
            } catch (InterruptedException exept) {
                exept.printStackTrace();
            }
        }
        executor.shutdown();
        //System.out.println("Crawler successfully completed work.");
    }

    public static void CrawlerTest(String initUrl, final int MAX_DEPTH, final int NUM_OF_THREADS, TaskCounter taskCounter, ConcurrentSkipListSet<String> pagesAlredyVisit) {
        final ExecutorService executor = Executors.newFixedThreadPool(NUM_OF_THREADS);

        taskCounter.inc();
        executor.execute(new Crawler(initUrl, taskCounter, pagesAlredyVisit,executor,0,MAX_DEPTH));

        while (taskCounter.getCount() != 0) {
            try {
                Thread.sleep(1);
            } catch (InterruptedException exept) {
                exept.printStackTrace();
            }
        }
        executor.shutdown();
        //System.out.println("Crawler successfully completed work.");
    }


}